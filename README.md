<img src="./logo.png" width="100px"/>

# Hướng dẫn làm bài Test

Để thực hiện bài test này, Học viên sẽ cần sử dụng [Visual Studio Code](https://code.visualstudio.com/), [Sublime Text](https://www.sublimetext.com/) và [Webtoolkitonline](https://www.webtoolkitonline.com/javascript-tester.html) để chạy code Javascript. Học viên sẽ download file [genbacoder-react-native-entry-test.md](./genbacoder-react-native-entry-test.md) từ Gitlab để bắt đầu làm bài

# Phương thức nộp bài làm

Sau khi kết thúc bài test, Học viên lưu lại bài làm của mình dưới dạng: HọTên-genbacoder-react-native-entry-test.MD và gửi đến địa chỉ: vinh.hoang@genbacoder.com
